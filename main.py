# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

saldo= 10000
opc = 0
while opc != 4:
    print ("\t.Cajero Automatico BAC. \n" +
    "1) Ingrese dinero en la cuenta \n"+
    "2) Retirar dinero en la cuenta \n"+
    "3) Mostrar dinero en la cuenta \n"+
    "4) salir \n"
           )
    opcion = int(input("Ingrese una opcion del menu \n"))

    print()
    if opcion ==1:
        extra = float(input("INGRESE EL MONTO :  "))
        saldo = saldo + extra
        print(f"TRANSACCION REALIZADA CORRECTAMENTE \nDinero en la cuenta: {saldo}")
    elif opcion == 2:
        retirar = float(input("INGRESE EL MONTO :  "))
        if retirar>saldo:
            print(f"TRANSACCION RECHAZADA POR FONDOS INSUFICIENTES \n SALDO DISPONIBLE: {saldo}")
        else:
            saldo -= retirar
            print(f"SALDO ACTUAL: {saldo}")
    elif opcion == 3:
        print(f"SALDO ACTUAL: {saldo}")
    elif opcion == 4:
        ("GRACIAS")
    else:
        print("Error ingreso mal, Verfique el menu ")